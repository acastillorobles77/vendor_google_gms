# vendor gapps

### How to use
 
Simply clone this repo on `vendor/google/gms` and add this line on your device makefile/ROM vendor: `$(call inherit-product-if-exists, vendor/google/gms/config.mk)`

Note: Only tested on LineageOS 19.1
